# **NEPPO - Teste de Lógica** #

Este teste é composto por 3 questões de lógica. Cada questão é dividida em:

* Descrição do cenário
* Exemplos de entrada
* Exemplos de saída

Você deve implementar uma solução, utilizando a linguagem que se sentir mais confortável, para resolver os problemas propostos.

Para todas as questões, não há a necessidade de se preocupar com entradas inválidas ou estouro do tipo de dado utilizado a não ser que o mesmo seja informado no próprio exercício.


## **1. Fórmula Misteriosa** ##

Muito além, nos confins inexplorados da região mais brega da Borda Ocidental desta Galáxia, há um pequeno sol amarelo e esquecido. 

Girando em torno deste sol, a uma distância de cerca de 148 milhões de quilômetros, há um planetinha verde-azulado absolutamente insignificante, cujas formas de vida, descendentes de primatas, são tão extraordinariamente primitivas que ainda acham que relógios digitais são uma grande idéia. 

Este planeta tem o seguinte problema: a maioria de seus habitantes estava quase sempre infeliz. Foram sugeridas muitas soluções para esse problema, mas a maior parte delas dizia respeito basicamente à movimentação de pequenos pedaços de papel colorido com números impressos, o que é curioso, já que no geral não eram os tais pedaços de papel colorido que se sentiam infelizes. 

E assim o problema continuava sem solução. Muitas pessoas eram más, e a maioria delas era muito infeliz, mesmo as que tinham relógios digitais. 
Um número cada vez maior de pessoas acreditava que havia sido um erro terrível da espécie descer das árvores. Algumas diziam que até mesmo subir nas árvores tinha sido uma péssima idéia, e que ninguém jamais deveria ter saído do mar. 

E, então, uma quinta-feira, mais de dois mil anos depois que um homem foi pregado num pedaço de madeira por ter dito que seria ótimo se as pessoas fossem legais umas com as outras para variar, um renomado matemático de nome Júpiter Apple, de repente compreendeu que, o significado da vida, do universo e tudo mais poderia ser calculado através de uma misteriosa fórmula que surgiu em sua mente em meio a um sonho que envolvia unicórnios e golfinhos voadores. 

**Ajude Júpiter a dar sentido à vida, ao universo e tudo mais. Para isso, implemente um algoritmo que receba como parâmetro de entrada um valor numérico, aplique a misteriosa fórmula ao valor em questão e retorne o valor computado.**

| Entrada (unsgined long) 	| Saída (unsgined long) 	|
|:-----------------------:	|:---------------------:	|
|            1            	|           11          	|
|            11           	|           21          	|
|            21           	|          1211         	|
|           1211          	|         111221        	|
|          111221         	|         312211        	|


## **2. Máquina do tempo** ##

Um dos maiores problemas encontrados em viajar no tempo não é vir a se tornar acidentalmente seu pai ou sua mãe. Não há nenhum problema em tornar-se seu próprio pai ou mãe com que uma família de mente aberta e bem ajustada não possa lidar. Não há tampouco problema em mudar o curso da história — o curso da história não muda porque todas as peças se juntam como num quebra-cabeça. Todas as mudanças importantes ocorreram antes das coisas que deveriam mudar e tudo dá na mesma no final. 

O problema maior é simplesmente gramatical, e a principal obra a ser consultada sobre esta questão é o tratado do Doutor e Senhor do tempo Charlie Brown Jr., Manual dos 1001 Tempos Gramaticais para o Viajante no Tempo. Ensina, por exemplo, a descrever algo que estava prestes a acontecer com você no passado antes de você evitá-lo pulando no tempo para dois dias depois com a intenção de evitá-lo.

O evento é descrito distintamente conforme você esteja referindo-se a ele do seu ponto natural no tempo, de uma época no futuro posterior ou numa época no passado posterior ao evento e posteriormente vai ficando mais e mais complicado caso você esteja viajando de cá para lá no tempo na tentativa de tornar-se seu próprio pai ou sua própria mãe.

A maioria dos leitores chega até o Futuro Semicondicional Subinvertido Plagal do Pretérito Subjuntivo Intencional antes de desistir; e de fato, em edição mais recente desse livro as páginas subseqüentes têm sido deixadas em branco para economizar custos de impressão.

Guias mais populares passam por cima desta abstração acadêmica, parando apenas numa nota lembrando que o termo "Futuro Perfeito" foi abandonado assim que se descobriu que não é.

Outro grande problema é, muitas das vezes, viajantes no tempo configuram suas maquinas para datas inexistentes, partindo assim em uma bela viajem com destino a inexistência.

 **Ajude a DMC, empresa que fabrica maquinas do tempo a desenvolver um algoritmo que valide datas, evitando assim, que sua clientela deixe de existir (literalmente).**

| Entrada (int int int) 	| Saída (boolean) 	|
|:---------------------:	|:---------------:	|
|       28 2 2017       	|       true      	|
|       29 2 2017       	|      false      	|
|       30 11 2017      	|       true      	|
|       31 11 2017      	|      false      	|

**Observações:** 

* **1.** Os três inteiros passados como parâmetros deverão estar na ordem: dia, mês e ano. 
* **2.** A data será validada conforme Calendário Gregoriano. a. O mês de fevereiro terá 29 dias em anos bissextos. b. Anos bissextos ocorrem a cada quatro anos (exceto anos múltiplos de 100 que não são múltiplos de 400).
* **3.** Não é permitida a utilização de qualquer implementação via importação para a manipulação/validação das datas (ex: java.util.Date). 

## **3. Garçons Modernos** ##

Uma vez que se tornou fácil e seguro viajar no tempo/espaço, sem correr o risco de se transformar em uma mosca humana, mudar fatos importantes da história ou até mesmo simplesmente passar a existir na inexistência.  Passou então a surgir problemas de cunho social.

 Entre os problemas reportados como gravíssimos e que irritavam arduamente a associação dos garçons modernos, e aqui não iremos explicar o motivo para o uso da expressão ‘moderno’, tendo em vista a complexidade de se usar termos temporais uma vez que se começa a manipular o tempo, era o fato dos romanos sempre pedirem cinco cervejas levantando apenas dois dedos.
 
**Ajude a associação dos garçons modernos a desenvolver um algoritmo que receba um número entre 1 e 3999 e retorne seu equivalente em romano.**

| Entrada (int) | Saída (String) |
|:-------------:|:--------------:|
| 4 | IV |
| 9 | IX |
| 40 | XL |
| 90 | XC |
| 400 | CD |
| 900 | CM |
| 1904 | MCMIV |
| 1954 | MCMLIV |
| 1990 | MCMXC |
| 2014 | MMXIV |

### **Observações: ** ###

**1. Símbolos romanos:** 

| Entrada (String) 	| Saída (int) 	|
|:-------------:	|:--------------:	|
|       I       	|        1       	|
|       V       	|        5       	|
|       X       	|       10       	|
|       L       	|       50       	|
|       C       	|       100      	|
|       D       	|       500      	|
|       M       	|      1000      	|

**2. Regras adicionais:**  

- I antes de V ou X indica um a menos (Ex: IV = 4). 
- X antes de L ou C indica dez a menos (Ex: XL = 40). 
- C antes de D ou M indica cem a menos (Ex: CD = 400).